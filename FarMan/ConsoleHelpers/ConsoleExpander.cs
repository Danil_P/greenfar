﻿using FarMan.FarEnums;
using FarMan.HelpStructures;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.ConsoleHelpers
{
    public static class ConsoleExpander
    {
        public static void WriteToPosition(int left, int top, char sign)
        {
            Console.SetCursorPosition(left, top);
            Console.Write(sign);
            SetCursorToDefault();
        }

        public static void WriteToPosition(int left, int top, string str)
        {
            Console.SetCursorPosition(left, top);
            Console.Write(str);
            SetCursorToDefault();
        }

        public static string CutString(string str, int maxLength)
        {
            if (str.Length <= maxLength)
                return str;
            else
                return str.Substring(0, maxLength - 2).Insert(maxLength - 2, "..");
        }

        public static void SetCursorToDefault()
        {
            Console.SetCursorPosition(InitParams.CursorDefaultPosition.x, InitParams.CursorDefaultPosition.y);
        }

        public static void DrawImage(string path, int startX, int startY)
        {
            Bitmap bitmap = new Bitmap(path);
            BitmapLockManager image = new BitmapLockManager(bitmap);
            image.LockBits();
            for (int i = startX; i < image.Width + startX; i++)
            {
                for (int j = startY; j < image.Height + startY; j++)
                {
                    Color color = image.GetPixel(i - startX, j - startY);
                    if (color.A != 0)
                    {
                        Console.BackgroundColor = ConvertColorToConsoleColor(color);
                        ConsoleExpander.WriteToPosition(i - startX + 1, j - startY + 1, ' ');
                    }
                }
            }
            image.UnlockBits();
        }

        public static void ClearConsole(int startX, int startY)
        {
            for (int i = 0; i < InitParams.Size[FarSizes.AppSize].width; i++)
            {
                for (int j = 0; j < InitParams.Size[FarSizes.AppSize].height; j++)
                {
                    Console.BackgroundColor = InitParams.Colors[FarColors.BackgroundColor];
                    ConsoleExpander.WriteToPosition(i, j, ' ');
                }
            }
        }

        public static ConsoleColor ConvertColorToConsoleColor(Color c)
        {
            int index = (c.R > 128 | c.G > 128 | c.B > 128) ? 8 : 0; // Bright bit
            index |= (c.R > 64) ? 4 : 0; // Red bit
            index |= (c.G > 64) ? 2 : 0; // Green bit
            index |= (c.B > 64) ? 1 : 0; // Blue bit
            return (ConsoleColor)index;

        }
    }
}
