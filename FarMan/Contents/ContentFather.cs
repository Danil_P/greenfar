﻿using FarMan.Controllers;
using FarMan.Interfaces;
using FarMan.Panels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.Contents
{
    public abstract class ContentFather : IKeyReactable, IInitializable
    {
        protected int width;
        protected int height;
        protected int startX;
        protected int startY;

        protected List<ConsoleKey> ReactKeys = new List<ConsoleKey>();

        protected IShaped panel;

        public ContentFather() { }

        public ContentFather(IShaped panel)
        {
            this.panel = panel;

            width = panel.Size.width;
            height = panel.Size.height;
            startX = panel.Location.x;
            startY = panel.Location.y;
        }

        public bool ReactOnKey(ConsoleKey key)
        {
            return ReactKeys.Contains(key);
        }

        public bool IsReactable()
        {
            return ReactKeys != null && ReactKeys.Count > 0;
        }

        public abstract void KeyDown();

        public abstract void KeyEnter();

        public abstract void KeyEscape();

        public abstract void KeyLeft();

        public abstract void KeyRight();

        public abstract void KeyUp();

        public abstract void Initialize();

        public abstract void KeyF2();

        public abstract void KeyF3();
    }
}
