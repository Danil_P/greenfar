﻿using FarMan.ConsoleHelpers;
using FarMan.FarEnums;
using FarMan.Panels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.Contents
{
    class FileCreatingContent : ContentFather
    {
        private ConsoleColor backgroundColor = InitParams.Colors[FarColors.BackgroundColor];
        private ConsoleColor staticTextColor = InitParams.Colors[FarColors.BorderColor];
        private ConsoleColor textColor = InitParams.Colors[FarColors.PlainTextColor];
        private string panelTitleDirectoryCreation = "Directory Creating";
        private string panelTitleFileCreation = "File Creating";

        public FileCreatingContent() { }

        public FileCreatingContent(IShaped panel) : base(panel) { }

        public override void Initialize()
        {
            EventRegistrator.Instance.RegisterNewItemCreateDir(CreateNewDir);
            EventRegistrator.Instance.RegisterNewItemCreateFile(CreateNewFile);
        }

        private void CreateNewFile(DirectoryInfo dir)
        {
            panel.Active = true;
            string previousPanelTitle = panel.Title;
            panel.Title = panelTitleFileCreation;
            panel.DrawShape();

            Console.BackgroundColor = backgroundColor;
            Console.ForegroundColor = staticTextColor;
            ConsoleExpander.WriteToPosition(startX + 1, startY + 1, "Create new file: ".PadRight(width - 1));

            Console.ForegroundColor = textColor;
            ConsoleExpander.WriteToPosition(startX + 1, startY + 2, "".PadRight(width - 1));

            Console.SetCursorPosition(startX + 1, startY + 2);

            string fileName = Console.ReadLine();

            FileInfo file = new FileInfo(dir.FullName + '\\' + fileName);
            file.Create();

            panel.Title = previousPanelTitle;
            panel.Active = false;
            panel.DrawShape();
            EventRegistrator.Instance.SetDefaultPanelActive();

        }

        public void CreateNewDir(DirectoryInfo dir)
        {
            panel.Active = true;
            string previousPanelTitle = panel.Title;
            panel.Title = panelTitleDirectoryCreation;
            panel.DrawShape();

            Console.BackgroundColor = backgroundColor;
            Console.ForegroundColor = staticTextColor;
            ConsoleExpander.WriteToPosition(startX + 1, startY + 1, "Create new folder: ".PadRight(width - 1));

            Console.ForegroundColor = textColor;
            ConsoleExpander.WriteToPosition(startX + 1, startY + 2, "".PadRight(width - 1));

            Console.SetCursorPosition(startX + 1, startY + 2);
            
            string dirName = Console.ReadLine();
            dir.CreateSubdirectory(dirName);

            panel.Title = previousPanelTitle;
            panel.Active = false;
            panel.DrawShape();
            EventRegistrator.Instance.SetDefaultPanelActive();
        }

        public override void KeyDown()
        {
            throw new NotImplementedException();
        }

        public override void KeyEnter()
        {
            throw new NotImplementedException();
        }

        public override void KeyEscape()
        {
            throw new NotImplementedException();
        }

        public override void KeyLeft()
        {
            throw new NotImplementedException();
        }

        public override void KeyRight()
        {
            throw new NotImplementedException();
        }

        public override void KeyUp()
        {
            throw new NotImplementedException();
        }

        public override void KeyF2()
        {
            throw new NotImplementedException();
        }

        public override void KeyF3()
        {
            throw new NotImplementedException();
        }
    }
}
