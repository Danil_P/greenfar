﻿using FarMan.ConsoleHelpers;
using FarMan.Panels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.Contents
{
    public class FileInfoContent : ContentFather
    {
        private string firstStr;
        private string secondStr;
        private string musicString = "";

        public FileInfoContent() { }

        public FileInfoContent(IShaped panel) : base(panel) { }

        public override void Initialize()
        {
            EventRegistrator.Instance.RegisterNewItemChanDir(OnDirectoryChanged);
            EventRegistrator.Instance.RegisterNewItemPlayMusic(OnMusicPlayed);
            EventRegistrator.Instance.RegisterNewItemStopMusic(OnMusicStop);
        }

        public void OnMusicPlayed(FileInfo file)
        {
            musicString = file.Name;
            Draw();
        }

        public void OnMusicStop(FileInfo file)
        {
            musicString = "";
        }

        public void OnDirectoryChanged(FileSystemInfo fileInfo)
        {
            if (panel.Title != "File Info")
            {
                panel.Title = "File Info";
                panel.DrawTitle();
            }
            if (fileInfo.GetType() == typeof(DirectoryInfo))
                fillDirInfo(fileInfo as DirectoryInfo);
            else
                fillFileInfo(fileInfo as FileInfo);
            Draw();
        }

        private void fillDirInfo(DirectoryInfo fileInfo)
        {
            string creationalTime = fileInfo.CreationTime.ToShortDateString();
            firstStr = String.Format("Created at: {0}", creationalTime);

            try
            {
                string numberOfFiles = fileInfo.GetFileSystemInfos().ToList<FileSystemInfo>().Count.ToString();
                secondStr = String.Format("Number of files: {0}", numberOfFiles);
            }
            catch
            {
                string name = fileInfo.Name;
                secondStr = String.Format("Name: {0}", name);
            }
        }

        private void fillFileInfo(FileInfo fileInfo)
        {
            string date = fileInfo.CreationTime.ToShortDateString();
            firstStr = String.Format("Created at: {0}", date);

            string extension = fileInfo.Extension;
            secondStr = String.Format("Exstension: {0}", extension);
        }

        public void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            ConsoleExpander.WriteToPosition(startX + 1, startY + 1, firstStr.PadRight(width - 1));
            ConsoleExpander.WriteToPosition(startX + 1, startY + 2, secondStr.PadRight(width - 1));
            Console.ForegroundColor = ConsoleColor.Yellow;
            int lastPosition = startX + width - musicString.Length - 1;
            ConsoleExpander.WriteToPosition(lastPosition, startY + 2, musicString);
        }

        public override void KeyDown()
        {
            throw new NotImplementedException();
        }

        public override void KeyEnter()
        {
            throw new NotImplementedException();
        }

        public override void KeyEscape()
        {
            throw new NotImplementedException();
        }

        public override void KeyLeft()
        {
            throw new NotImplementedException();
        }

        public override void KeyRight()
        {
            throw new NotImplementedException();
        }

        public override void KeyUp()
        {
            throw new NotImplementedException();
        }

        public override void KeyF2()
        {
            throw new NotImplementedException();
        }

        public override void KeyF3()
        {
            throw new NotImplementedException();
        }
    }
}
