﻿using FarMan.ConsoleHelpers;
using FarMan.Contents.FileListSpace;
using FarMan.Controllers;
using FarMan.HelpStructures;
using FarMan.Panels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.Contents
{
    public class FileList : ContentFather
    {

        public int FinishX { get { return width + startX; } }
        public int FinishY { get { return height + startY; } }

        public FileList(IShaped panel) : base(panel)
        {
            ReactKeys = new List<ConsoleKey>() {
                ConsoleKey.DownArrow,
                ConsoleKey.UpArrow,
                ConsoleKey.Enter,
                ConsoleKey.F2,
                ConsoleKey.F3
            };

        }

        private DirElementManager dirManager;
        private DirectoryInfo currentDir = null;

        public override void Initialize()
        {
            DirectoryInfo dirInfo = new DirectoryInfo(@"C:\");
            currentDir = dirInfo;

            List<FileSystemInfo> fileInfo = dirInfo.GetFileSystemInfos().ToList<FileSystemInfo>();

            if (dirInfo.Parent != null)
                fileInfo.Insert(0, dirInfo.Parent);

            dirManager = new DirElementManager(
                fileInfo,
                new Location(startX + 1, startY + 1),
                new Size(width - 1, height - 1)
           );

            dirManager.DrawElements();

            panel.DrawShape();
        }

        public override void KeyUp()
        {
            dirManager.Previous();
        }

        public override void KeyDown()
        {
            dirManager.Next();
        }

        public override void KeyEnter()
        {
            if (dirManager.Current.GetType() == typeof(DirectoryInfo))
                EnteredDirectory(dirManager.Current as DirectoryInfo);

            if (dirManager.Current.GetType() == typeof(FileInfo))
            {
                EnteredFile(dirManager.Current as FileInfo);
                EventRegistrator.Instance.OnEnterFile(dirManager.Current as FileInfo);
            }
        }

        protected void EnteredDirectory(DirectoryInfo dirInfo)
        {
            try
            {
                currentDir = dirInfo;
                List<FileSystemInfo> systemDirInfo = dirInfo.GetFileSystemInfos().ToList<FileSystemInfo>();

                if (dirInfo.Parent != null)
                    systemDirInfo.Insert(0, dirInfo.Parent);

                panel.Title = dirInfo.FullName;
                panel.DrawTitle();
                dirManager.ChangeDir(systemDirInfo);
            }
            catch { }
        }

        protected void EnteredFile(FileInfo fileInfo)
        {
            EventRegistrator.Instance.OnEnterFile(fileInfo);
        }

        public override void KeyF2()
        {
            EventRegistrator.Instance.OnCreateDir(currentDir);
            EnteredDirectory(currentDir);
            panel.DrawShape();

        }

        public override void KeyF3()
        {
            EventRegistrator.Instance.OnCreateFile(currentDir);
            EnteredDirectory(currentDir);
            panel.DrawShape();
        }


        public override void KeyEscape()
        {
            throw new NotImplementedException();
        }

        public override void KeyLeft()
        {
            throw new NotImplementedException();
        }

        public override void KeyRight()
        {
            throw new NotImplementedException();
        }
    }
}
