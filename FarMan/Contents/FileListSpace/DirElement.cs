﻿using FarMan.ConsoleHelpers;
using FarMan.FarEnums;
using FarMan.HelpStructures;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FarMan.Contents.FileListSpace
{
    public class DirElement
    {
        public bool isActive = false;

        private ConsoleColor textColor = InitParams.DirElementsColors[DirElementColor.File];
        private ConsoleColor activeTextColor = InitParams.DirElementsColors[DirElementColor.ActiveElementText];

        private ConsoleColor backgroundColor = InitParams.Colors[FarColors.BackgroundColor];
        private ConsoleColor activeBackgroundColor = InitParams.DirElementsColors[DirElementColor.ActiveElementBackground];

        private Location position;
        private int length;
        private FileSystemInfo _element;
        public FileSystemInfo Element {
            get { return _element; }
            set
            {
                _element = value;
                if(_element != null && _element.GetType() == typeof(DirectoryInfo))
                {
                    try
                    {
                        (_element as DirectoryInfo).GetFiles();
                        textColor = InitParams.DirElementsColors[DirElementColor.File];
                    } catch(UnauthorizedAccessException)
                    {
                        textColor = ConsoleColor.Gray;
                    }
                } else
                {
                    textColor = InitParams.DirElementsColors[DirElementColor.File];
                }
            }
        }

        public bool enabled = false;

        private ConsoleColor CurrentTextColor {
            get {
                return isActive ? activeTextColor : textColor;
            }
        }

        private ConsoleColor BackgroundColor
        {
            get
            {
                return isActive ? activeBackgroundColor : backgroundColor;
            }
        }

        private string ActiveText
        {
            get { return Text.PadRight(length); }
        }

        private string Text
        {
            get
            {
                string directorySymbol = (Element.GetType() == typeof(DirectoryInfo) ? " ■ " : " · ");
                return ConsoleExpander.CutString(directorySymbol + Element.Name, length - 2);
            }
        }

        public DirElement() { }

        public DirElement(FileSystemInfo element, Location position, int length)
        {
            this.Element = element;
            this.position = position;
            this.length = length;
        }

        public void Draw()
        {
            Console.ForegroundColor = CurrentTextColor;
            Console.BackgroundColor = BackgroundColor;
            string viewText = enabled ? ActiveText : "         ".PadRight(length);
            ConsoleExpander.WriteToPosition(position.x, position.y, viewText);
        }
    }
}
