﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.Contents.FileListSpace
{
    public enum DirElementColor
    {
        OpenFolder,
        ClosedFolder,
        File,
        ActiveElementText,
        ActiveElementBackground
    }
}
