﻿using FarMan.HelpStructures;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.Contents.FileListSpace
{
    public class DirElementManager
    {

        private List<FileSystemInfo> fileInfo;
        private List<DirElement> dirElements = new List<DirElement>();
        private int upBound = 0;
        private int downBound = 0;
        private Location location;
        private Size size;

        private int _current = 0;
        public int current {
            get { return _current; }
            set {
                _current = value;
                EventRegistrator.Instance.OnChangeCurrentDir(Current);
            }
        }
        public int currentDirEl = 0;

        public FileSystemInfo Current
        {
            get { return fileInfo[current]; }
        }

        //max - 10 min - 1
        private int EnableDirElemsNum
        {
            get
            {
                return Math.Min(fileInfo.Count, dirElements.Count);
            }
        }


        public DirElementManager() { }

        public DirElementManager(List<FileSystemInfo> fileInfo, Location location, Size size)
        {
            this.size = size;
            this.location = location;
            InitDirElements();
            
            ChangeDir(fileInfo);
        }

        public void ChangeDir(List<FileSystemInfo> fileInfo)
        {
            this.fileInfo = fileInfo;
           
            upBound = 0;
            downBound = Math.Min(fileInfo.Count, EnableDirElemsNum);

            ChangeCurrent(0);
            
            FillDirElements();
            DrawElements();
        }

        private void InitDirElements()
        {
            int leftMargin = location.y;
            int topMargin = location.x;
            for (int i = 0; i < size.height; i++)
            {
                dirElements.Add(new DirElement(
                    null,
                    new Location(leftMargin, topMargin + i),
                    size.width)
                );
            }
        }

        public void FillDirElements()
        {
            
            for (int i = 0; i < Math.Min(dirElements.Count, fileInfo.Count); i++)
            {
                dirElements[i].Element = fileInfo[i];
                dirElements[i].enabled = true;
            }

            for (int i = EnableDirElemsNum; i < dirElements.Count; i++)
            {
                dirElements[i].enabled = false;
            }
        }

        public void DrawElements()
        {
            dirElements[currentDirEl].isActive = true;
            foreach (var elem in dirElements)
            {
                elem.Draw();
            }

        }

        public FileSystemInfo Next()
        {
            dirElements[currentDirEl].isActive = false;
            dirElements[currentDirEl].Draw();

            current = (current + 1) % fileInfo.Count;

            if (currentDirEl == EnableDirElemsNum - 1 && current >= EnableDirElemsNum)
            {
                upBound++;
                downBound++;
                MoveDirElements();
            }
            else if (currentDirEl == EnableDirElemsNum - 1 && current == 0)
            {
                upBound = 0;
                downBound = Math.Min(fileInfo.Count, EnableDirElemsNum);
                currentDirEl = 0;
                MoveDirElements();
            }
            else
            {
                currentDirEl = (currentDirEl + 1) % EnableDirElemsNum;
            }

            dirElements[currentDirEl].isActive = true;
            dirElements[currentDirEl].Draw();
            return fileInfo[current];
        }

        public FileSystemInfo Previous()
        {
            dirElements[currentDirEl].isActive = false;
            dirElements[currentDirEl].Draw();

            current = (current == 0) ? fileInfo.Count - 1 : current - 1;

            if (currentDirEl == 0 && current == fileInfo.Count - 1)
            {
                downBound = fileInfo.Count;
                upBound = downBound - EnableDirElemsNum;
                currentDirEl = EnableDirElemsNum - 1;
                MoveDirElements();
            }
            else if(currentDirEl == 0 && upBound >= 0)
            {
                upBound--;
                downBound--;
                MoveDirElements();
            }
            else
            {
                currentDirEl = (currentDirEl == 0) ? EnableDirElemsNum - 1 : currentDirEl - 1;
            }

            dirElements[currentDirEl].isActive = true;
            dirElements[currentDirEl].Draw();
            return fileInfo[current];
        }

        private void ChangeCurrent(int newCurrent)
        {
            if (newCurrent >= EnableDirElemsNum)
                return;
            dirElements[currentDirEl].isActive = false;
            currentDirEl = newCurrent;
            current = newCurrent;
            dirElements[currentDirEl].isActive = true;
        }

        void MoveDirElements()
        {
            for(int i = upBound, j = 0; i < downBound; i++, j++)
            {
                dirElements[j].Element = fileInfo[i];
                dirElements[j].Draw();
            }
        }
    }
}
