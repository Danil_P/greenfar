﻿using FarMan.ConsoleHelpers;
using FarMan.Panels;
using FarMan.HelpStructures;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.Contents.FileRepresentation
{
    public class FileImageContent : ContentFather
    {
        readonly private string[] imgExtensions = { ".png", ".jpeg", ".jpg" };

        public FileImageContent() { }

        public FileImageContent(IShaped panel) : base(panel) { }

        public override void Initialize()
        {
            EventRegistrator.Instance.RegisterNewItemChanDir(OnDirectoryChanged);
        }

        public void OnDirectoryChanged(FileSystemInfo fileInfo)
        {
            if (fileInfo.GetType() == typeof(FileInfo))
            {
                FileInfo file = fileInfo as FileInfo;
                if (imgExtensions.Contains(file.Extension))
                {
                    Bitmap image = new Bitmap(Image.FromFile(file.FullName));

                    Bitmap resizedImage = ResizeImage(image);

                    DrawImage(resizedImage);
                }
            }

        }

        public void DrawImage(Bitmap image)
        {
            for(int i = startX; i < image.Width + startX; i++)
            {
                for(int j = startY; j < image.Height + startY; j++)
                {

                    Console.BackgroundColor = ConsoleExpander.
                        ConvertColorToConsoleColor(image.GetPixel(i - startX, j - startY));
                    ConsoleExpander.WriteToPosition(i + 1, j + 1, ' ');

                }
            }
        }

        private Bitmap ResizeImage(Bitmap originalImage)
        {
            var originalImageLM =  new BitmapLockManager(originalImage);
            int groupedPixelsWidth = (originalImage.Width) / (width - 1);
            int groupedPixelHeight = (originalImage.Height) / (height - 1);

            Bitmap resizedImage = new Bitmap(width - 1, height - 1);
            var resizedImageLM = new BitmapLockManager(resizedImage);

            originalImageLM.LockBits();
            resizedImageLM.LockBits();

            int numberOfMiniGroupsWidth = 0;
            for(int i = 0; i + groupedPixelsWidth < width*groupedPixelsWidth; i += groupedPixelsWidth)
            {
                int numberOfMiniGroupsHeight = 0;
                for(int j = 0; j + groupedPixelHeight < height*groupedPixelHeight; j += groupedPixelHeight)
                {
                    int redColorValue = 0;
                    int blueColorValue = 0;
                    int greenColorValue = 0;
                    
                    for(int miniWidth = i; miniWidth < i + groupedPixelsWidth; miniWidth++)
                    {
                        for (int miniHeight = j; miniHeight < j + groupedPixelHeight; miniHeight++)
                        {
                            Color pixelColor = originalImageLM.GetPixel(miniWidth, miniHeight);
                            redColorValue += pixelColor.R;
                            blueColorValue += pixelColor.B;
                            greenColorValue += pixelColor.G;
                        }
                    }

                    int pixelsInGroup = groupedPixelHeight * groupedPixelsWidth;
                    var avrgCol = Color.FromArgb(redColorValue / pixelsInGroup,
                        greenColorValue / pixelsInGroup,
                        blueColorValue / pixelsInGroup);
                   
                    resizedImageLM.SetPixel(numberOfMiniGroupsWidth, numberOfMiniGroupsHeight, avrgCol);
                   
                    numberOfMiniGroupsHeight++;
                }
                numberOfMiniGroupsWidth++;
            }
            originalImageLM.UnlockBits();
            resizedImageLM.UnlockBits();

            return resizedImage;
        }

        public override void KeyDown()
        {
            throw new NotImplementedException();
        }

        public override void KeyEnter()
        {
            throw new NotImplementedException();
        }

        public override void KeyEscape()
        {
            throw new NotImplementedException();
        }

        public override void KeyLeft()
        {
            throw new NotImplementedException();
        }

        public override void KeyRight()
        {
            throw new NotImplementedException();
        }

        public override void KeyUp()
        {
            throw new NotImplementedException();
        }

        public override void KeyF2()
        {
            throw new NotImplementedException();
        }

        public override void KeyF3()
        {
            throw new NotImplementedException();
        }
    }
}
