﻿using FarMan.ConsoleHelpers;
using FarMan.FarEnums;
using FarMan.HelpStructures;
using FarMan.Panels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.Contents.FileRepresentation
{
    public class SystemInfoIconsContent : ContentFather
    {

        private readonly Dictionary<string[], string> extensionPicture = 
            new Dictionary<string[], string>()
            {
                { new string[] {".txt", ".doc", ".docx", ".pdf" }, "textformats.png" },
                { new string[] {".exe"}, "exeformats.png" },
                { new string[] {".mp3", ".wav" }, "audioformats.png" },
                { new string[] {".mkv", ".avi" }, "videoformats.png" }
            };
        private readonly string directoryIconPath = "folderIcon.png";
        private readonly string defaultIconPath = "defaultIcon.png";
        private readonly string iconsFolder = "../icons/";

        private string lastDrawnIconPath = "";

        public SystemInfoIconsContent(IShaped panel) : base(panel) { }

        public override void Initialize()
        {
            EventRegistrator.Instance.RegisterNewItemChanDir(OnDirectoryChanged);
        }

        public void OnDirectoryChanged(FileSystemInfo fileInfo)
        {
            string iconPath = GetIconByExtension(fileInfo);
            if (iconPath == lastDrawnIconPath)
                return;
            else
                lastDrawnIconPath = iconPath;
            try
            {
                ClearField();
                Bitmap image = new Bitmap(Image.FromFile(iconsFolder + iconPath));
                DrawImage(image);
            }
            catch { }
        }

        public void DrawImage(Bitmap image)
        {
            BitmapLockManager bitmapLM = new BitmapLockManager(image);
            bitmapLM.LockBits();
            for (int i = startX; i < bitmapLM.Width + startX; i++)
            {
                for (int j = startY; j < bitmapLM.Height + startY; j++)
                {
                    Color currentColor = bitmapLM.GetPixel(i - startX, j - startY);
                    if (currentColor.A != 0)
                    {
                        Console.BackgroundColor = ConsoleColor.Yellow;
                        ConsoleExpander.WriteToPosition(i + 1, j + 1, ' ');
                    }
                }
            }
            bitmapLM.UnlockBits();
        }

        public void ClearField()
        {
            Console.BackgroundColor = InitParams.Colors[FarColors.BackgroundColor];
            for (int j = startY + 1; j < height; j++)
            {
                ConsoleExpander.WriteToPosition(startX + 1, j, "".PadRight(width-1));
            }
        }

        private string GetIconByExtension(FileSystemInfo fileSystemInfo)
        {
            if( fileSystemInfo.GetType() == typeof(DirectoryInfo))
            {
                return directoryIconPath;
            } else
            {
                FileInfo file = fileSystemInfo as FileInfo;
                string extension = file.Extension;
                foreach(var item in extensionPicture)
                {
                    if (item.Key.Contains(extension))
                        return item.Value;
                }
            }
            return defaultIconPath;
        }

        public override void KeyDown()
        {
            throw new NotImplementedException();
        }

        public override void KeyEnter()
        {
            throw new NotImplementedException();
        }

        public override void KeyEscape()
        {
            throw new NotImplementedException();
        }

        public override void KeyF2()
        {
            throw new NotImplementedException();
        }

        public override void KeyF3()
        {
            throw new NotImplementedException();
        }

        public override void KeyLeft()
        {
            throw new NotImplementedException();
        }

        public override void KeyRight()
        {
            throw new NotImplementedException();
        }

        public override void KeyUp()
        {
            throw new NotImplementedException();
        }
    }
}
