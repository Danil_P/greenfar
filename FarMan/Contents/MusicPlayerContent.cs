﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FarMan.Contents
{
    public class MusicPlayerContent
    {
        private static string currentPlayingFile = null;
        private static SoundPlayer player;

        private readonly string[] availableFormats = new string[]{ ".wav" };

        public void Initialize()
        {
            EventRegistrator.Instance.RegisterNewItemEnteredFile(OnEnteredFile);
            player = new SoundPlayer();
        }

        public void OnEnteredFile(FileInfo file)
        {
            if (!availableFormats.Contains(file.Extension))
                return;

            EventRegistrator.Instance.OnStopMusic(file);
            
            currentPlayingFile = file.FullName;
            player.SoundLocation = currentPlayingFile;
            EventRegistrator.Instance.OnPlayMusic(file);
            new Thread(new ThreadStart(LaunchMusic)).Start();
        }

        private static void LaunchMusic()
        {
            player.Play();
        }
    }
}
