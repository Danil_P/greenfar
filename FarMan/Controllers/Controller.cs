﻿using FarMan.Contents;
using FarMan.Panels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.Controllers
{
    public class Controller
    {

        ContentFather content;
        public ContentFather Content {
            set {
                content = value;
                content.Initialize();
            }
        }

        public Controller() { }

        public Controller(ContentFather content)
        {
            this.content = content;
            content.Initialize();
        }

        public void KeyDown()
        {
            if (!content.ReactOnKey(ConsoleKey.DownArrow))
                return;
            content.KeyDown();
        }

        public void KeyEnter()
        {
            if (!content.ReactOnKey(ConsoleKey.Enter))
                return;
            content.KeyEnter();
        }

        public void KeyEscape()
        {
            if (!content.ReactOnKey(ConsoleKey.Escape))
                return;
            content.KeyEscape();
        }

        public void KeyLeft()
        {
            if (!content.ReactOnKey(ConsoleKey.LeftArrow))
                return;
            content.KeyLeft();
        }

        public void KeyRight()
        {
            if (!content.ReactOnKey(ConsoleKey.RightArrow))
                return;
            content.KeyRight();
        }

        public void KeyUp()
        {
            if (!content.ReactOnKey(ConsoleKey.UpArrow))
                return;
            content.KeyUp();
        }

        public void KeyF2()
        {
            if (!content.ReactOnKey(ConsoleKey.F2))
                return;
            content.KeyF2();
        }

        public void KeyF3()
        {
            if (!content.ReactOnKey(ConsoleKey.F3))
                return;
            content.KeyF3();
        }
    }
}
