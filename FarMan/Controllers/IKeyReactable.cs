﻿using System;

namespace FarMan.Controllers
{
    public interface IKeyReactable
    {
        void KeyUp();
        void KeyDown();
        void KeyLeft();
        void KeyRight();
        void KeyEnter();
        void KeyEscape();
        bool ReactOnKey(ConsoleKey key);
    }
}

