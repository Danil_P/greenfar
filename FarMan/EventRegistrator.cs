﻿using FarMan.Panels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan
{
    public class EventRegistrator
    {
        private static EventRegistrator instance;
        public static EventRegistrator Instance
        {
            get {
                if (instance == null)
                {
                    instance = new EventRegistrator();
                }
                return instance;
            }
        }

        private EventRegistrator() { }

        public delegate void CurrentSysinfoReaction(FileSystemInfo sysInfo);
        public delegate void CurrentFileInfoReaction(FileInfo fileInfo);
        public delegate void CurrentDirInfoReaction(DirectoryInfo fileInfo);

        #region Items Listing

        private List<CurrentSysinfoReaction> reactItemsChangedDir = new List<CurrentSysinfoReaction>();

        public void RegisterNewItemChanDir(CurrentSysinfoReaction reactFunction)
        {
            reactItemsChangedDir.Add(reactFunction);
        }

        public void OnChangeCurrentDir(FileSystemInfo fileInfo)
        {
            foreach(var item in reactItemsChangedDir)
            {
                item(fileInfo);
            }
        }
        #endregion

        #region Enter File
        private List<CurrentFileInfoReaction> reactItemsEnteredFile = new List<CurrentFileInfoReaction>();

        public void RegisterNewItemEnteredFile(CurrentFileInfoReaction reactFunction)
        {
            reactItemsEnteredFile.Add(reactFunction);
        }

        public void OnEnterFile(FileInfo fileInfo)
        {
            foreach (var item in reactItemsEnteredFile)
            {
                item(fileInfo);
            }
        }
        #endregion

        #region create dir
        private List<CurrentDirInfoReaction> createDir = new List<CurrentDirInfoReaction>();

        public void RegisterNewItemCreateDir(CurrentDirInfoReaction reactFunction)
        {
            createDir.Add(reactFunction);
        }

        public void OnCreateDir(DirectoryInfo dir)
        {
            foreach (var item in createDir)
            {
                item(dir);
            }
        }
        #endregion

        #region create file
        private List<CurrentDirInfoReaction> createFile = new List<CurrentDirInfoReaction>();

        public void RegisterNewItemCreateFile(CurrentDirInfoReaction reactFunction)
        {
            createFile.Add(reactFunction);
        }

        public void OnCreateFile(DirectoryInfo dir)
        {
            foreach (var item in createFile)
            {
                item(dir);
            }
        }
        #endregion

        #region active panel
        private List<IShaped> allPanels = new List<IShaped>();
        public IShaped DefaultPanel { private get; set; }

        public void SetDefaultPanelActive()
        {
            DefaultPanel.Active = true;
            DefaultPanel.DrawShape();
        }

        public void RegisterNewItemPanel(IShaped panel)
        {
            allPanels.Add(panel);
        }

        public void OnChangedActive(IShaped panel)
        {
            foreach (var itemPanel in allPanels)
            {
                if (itemPanel.Active == true && itemPanel != panel)
                {
                    itemPanel.Active = false;
                    itemPanel.DrawShape();
                }
            }
        }
        #endregion

        #region play music
        private List<CurrentFileInfoReaction> reactItemsPlayMusic = new List<CurrentFileInfoReaction>();

        public void RegisterNewItemPlayMusic(CurrentFileInfoReaction reactFunction)
        {
            reactItemsPlayMusic.Add(reactFunction);
        }

        public void OnPlayMusic(FileInfo fileInfo)
        {
            foreach (var item in reactItemsPlayMusic)
            {
                item(fileInfo);
            }
        }
        #endregion

        #region stop music
        private List<CurrentFileInfoReaction> reactItemsStopMusic = new List<CurrentFileInfoReaction>();

        public void RegisterNewItemStopMusic(CurrentFileInfoReaction reactFunction)
        {
            reactItemsStopMusic.Add(reactFunction);
        }

        public void OnStopMusic(FileInfo fileInfo)
        {
            foreach (var item in reactItemsStopMusic)
            {
                item(fileInfo);
            }
        }
        #endregion
    }
}
