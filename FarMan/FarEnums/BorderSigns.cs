﻿namespace FarMan.FarEnums
{
    public enum BorderSigns
    {
        Vertical,
        Horizontal,
        LeftTop,
        LeftBottom,
        RightBottom,
        RightTop
    }
}
