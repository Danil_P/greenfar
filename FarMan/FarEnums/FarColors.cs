﻿namespace FarMan.FarEnums
{
    public enum FarColors
    {
        BackgroundColor,
        BackgroundColorActive,
        PlainTextColor,
        TextColorActive,
        BorderColor,
        ActiveBorderColor,
        BorderTitleColor,
    }
}
