﻿namespace FarMan.FarEnums
{
    public enum FarLocations
    {
        PanelFileListenerLocation,
        PanelFileViewLocation,
        PanelFileDataLocation
    }
}
