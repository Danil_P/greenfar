﻿namespace FarMan.FarEnums
{
    public enum FarSizes
    {
        AppSize,
        PanelFileListenerSize,
        PanelFileViewSize,
        PanelFileDataSize,
    }
}
