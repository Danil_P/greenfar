﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.HelpStructures
{
    public class BorderCharacters
    {
        public char vertical;
        public char horizontal;

        public char leftTop;
        public char leftBottom;
        public char rightTop;
        public char rightBottom;

        public BorderCharacters(char border)
        {
            vertical = border;
            horizontal = border;
            leftBottom = border;
            leftTop = border;
            rightBottom = border;
            rightTop = border;
        }

        public BorderCharacters(char vertical, char horizontal, 
            char leftTop, char leftBottom, char rightTop, char rightBottom)
        {
            this.vertical = vertical;
            this.horizontal = horizontal;
            this.leftTop = leftTop;
            this.leftBottom = leftBottom;
            this.rightTop = rightTop;
            this.rightBottom = rightBottom;
        }
    }
}
