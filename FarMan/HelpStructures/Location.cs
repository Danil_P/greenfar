﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.HelpStructures
{
    public class Location
    {
        public int x = 0;
        public int y = 0;

        public Location(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
