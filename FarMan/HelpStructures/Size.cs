﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarMan.HelpStructures
{
    public class Size
    {
        public int width = 0;
        public int height = 0;

        public Size(int width, int height)
        {
            this.width = width;
            this.height = height;
        }
    }
}
