﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarMan.HelpStructures;
using FarMan.FarEnums;
using FarMan.Contents.FileListSpace;

namespace FarMan
{
    public static class InitParams
    {
        /// <summary>
        /// Colors of all elements
        /// </summary>
        public readonly static Dictionary<FarColors, ConsoleColor> Colors =
            new Dictionary<FarColors, ConsoleColor>()
        {
                { FarColors.ActiveBorderColor, ConsoleColor.Yellow },
                { FarColors.BorderColor, ConsoleColor.Gray },
                { FarColors.BorderTitleColor, ConsoleColor.Black },
                { FarColors.TextColorActive, ConsoleColor.DarkYellow },
                { FarColors.PlainTextColor, ConsoleColor.White },
                { FarColors.BackgroundColorActive, ConsoleColor.Blue },
                { FarColors.BackgroundColor, ConsoleColor.DarkGreen },
        };

        /// <summary>
        /// Colors for Files representation
        /// </summary>
        public readonly static Dictionary<DirElementColor, ConsoleColor> DirElementsColors =
            new Dictionary<DirElementColor, ConsoleColor>()
            {
                { DirElementColor.OpenFolder, ConsoleColor.White },
                { DirElementColor.ClosedFolder, ConsoleColor.DarkGray },
                { DirElementColor.File, ConsoleColor.White },
                { DirElementColor.ActiveElementBackground, ConsoleColor.Green },
                { DirElementColor.ActiveElementText, ConsoleColor.Black }
            };
        
        /// <summary>
        /// Sizes of all elements
        /// </summary>
        public readonly static Dictionary<FarSizes, Size> Size =
            new Dictionary<FarSizes, Size>()
            {
                { FarSizes.AppSize, new Size(92, 26) },
                { FarSizes.PanelFileListenerSize, new Size(30, 24) },
                { FarSizes.PanelFileViewSize, new Size(59, 20) },
                { FarSizes.PanelFileDataSize, new Size(59, 3) }
            };

        /// <summary>
        /// Locations of all elements
        /// </summary>
        public readonly static Dictionary<FarLocations, Location> Location =
            new Dictionary<FarLocations, HelpStructures.Location>()
            {
                { FarLocations.PanelFileListenerLocation, new Location(0, 0) },
                { FarLocations.PanelFileViewLocation, new Location(31, 0) },
                { FarLocations.PanelFileDataLocation, new Location(31, 21) }
            };

        /// <summary>
        /// Border signs
        /// </summary>
        public readonly static Dictionary<BorderSigns, char> BorderSign =
            new Dictionary<BorderSigns, char>()
            {
                { BorderSigns.Horizontal, '═' },
                { BorderSigns.Vertical, '║' },
                { BorderSigns.LeftBottom, '╚' },
                { BorderSigns.RightBottom, '╝' },
                { BorderSigns.LeftTop, '╔'},
                { BorderSigns.RightTop, '╗' }
            };

        public readonly static Location CursorDefaultPosition = new Location(91, 25);
    }
}
