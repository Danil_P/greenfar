﻿namespace FarMan.Interfaces
{
    public interface IInitializable
    {
        void Initialize();
    }
}
