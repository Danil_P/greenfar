﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarMan.HelpStructures;

namespace FarMan.Panels
{
    public interface IShaped
    {
        
        Location Location { get; set; }
        Size Size { get; set; }
        BorderCharacters BorderSigns { set; }
        bool IsHidden { get; }
        ConsoleColor BackroundColor { set; }
        ConsoleColor BorderColor { set; }
        ConsoleColor ActiveBorderColor { set; }
        string Title { get; set; }
        bool Active { get; set; }

        void DrawShape();
        void DrawTitle();
        void Hide();
        void Show();
    }
}
