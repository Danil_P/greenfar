﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarMan.HelpStructures;
using FarMan.ConsoleHelpers;

namespace FarMan.Panels
{
    public partial class Panel : IShaped
    {
        #region size and position
        private Location location;
        public Location Location {
            get { return location; }
            set { location = value; }
        }

        private Size size;
        public Size Size
        {
            get { return size; }
            set { size = value; }
        }
        #endregion

        #region graphical design
        private ConsoleColor backgroundColor;
        public ConsoleColor BackroundColor {
            set { backgroundColor = value; }
        }

        private ConsoleColor borderColor;
        public ConsoleColor BorderColor {
            set { borderColor = value; }
        }

        private BorderCharacters borderSigns;
        public BorderCharacters BorderSigns
        {
            set { borderSigns = value; }
        }

        private ConsoleColor activeBorderColor;
        public ConsoleColor ActiveBorderColor { set { activeBorderColor = value; } }

        private ConsoleColor titleColor;
        public ConsoleColor TitleColor { set { titleColor = value;  } }
        #endregion

        #region title
        public string title = "";

        public string Title
        {
            get
            {
                return title;
            }

            set
            {
                title = value;
            }
        }

        private string FormattedTitle
        {
            get {
                if (title.Length <= Size.width - 2)
                    return title;
                else
                    return title.Substring(0, Size.width - 4).Insert(Size.width - 4, "..");
            }
        }

        private int XPositionForTitle
        {
            get {
                if (title.Length <= Size.width - 2)
                    return Location.x + (Size.width - title.Length) / 2;
                else
                    return Location.x + 1;
            }
        }
        #endregion

        bool isHidden;
        public bool IsHidden {
            get { return isHidden; }
        }

        private bool active;

        public ConsoleColor currentBorderColor {
            get { return active ? activeBorderColor : borderColor; }
        }

        public bool Active
        {
            get { return active; }
            set
            {
                if(value == true)
                    EventRegistrator.Instance.OnChangedActive(this);
                active = value;
                DrawShape();
            }
        }

        #region constructors
        public Panel() { }

        public Panel(Location location, Size size)
        {
            this.location = location;
            this.size = size;
        }

        public Panel(Location location, Size size, string title)
        {
            this.location = location;
            this.size = size;
            this.title = title;
        }
        #endregion

        public void Hide()
        {
            isHidden = true;
        }

        public void Show()
        {
            isHidden = false;
        }

        public void DrawShape()
        {

            Console.BackgroundColor = backgroundColor;
            Console.ForegroundColor = currentBorderColor;
            for (int i = Location.y; i <= Location.y + Size.height; i++)
            {
                ConsoleExpander.WriteToPosition(Location.x, i, borderSigns.vertical);
                ConsoleExpander.WriteToPosition(Location.x + Size.width, i, borderSigns.vertical);
            }

            for (int i = Location.x + 1; i <= Location.x + Size.width; i++)
            {
                ConsoleExpander.WriteToPosition(i, Location.y, borderSigns.horizontal);
                ConsoleExpander.WriteToPosition(i, Location.y + Size.height, borderSigns.horizontal);
            }

            ConsoleExpander.WriteToPosition(Location.x, Location.y, borderSigns.leftTop);
            ConsoleExpander.WriteToPosition(Location.x + Size.width, Location.y, borderSigns.rightTop);
            ConsoleExpander.WriteToPosition(Location.x, Location.y + Size.height, borderSigns.leftBottom);
            ConsoleExpander.WriteToPosition(Location.x + Size.width, Location.y + Size.height, 
                borderSigns.rightBottom);

            Console.ForegroundColor = titleColor;
            Console.BackgroundColor = currentBorderColor;
            ConsoleExpander.WriteToPosition(XPositionForTitle, Location.y, FormattedTitle);
        }

        public void DrawTitle()
        {
            Console.BackgroundColor = backgroundColor;
            Console.ForegroundColor = currentBorderColor;

            for (int i = Location.x + 1; i <= Location.x + Size.width; i++)
            {
                ConsoleExpander.WriteToPosition(i, Location.y, borderSigns.horizontal);
            }

            ConsoleExpander.WriteToPosition(Location.x, Location.y, borderSigns.leftTop);
            ConsoleExpander.WriteToPosition(Location.x + Size.width, Location.y, borderSigns.rightTop);

            Console.ForegroundColor = titleColor;
            Console.BackgroundColor = currentBorderColor;
            ConsoleExpander.WriteToPosition(XPositionForTitle, Location.y, FormattedTitle);
        }

    }
}
