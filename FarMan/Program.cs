﻿using System;
using System.Collections.Generic;
using FarMan.Panels;
using FarMan.FarEnums;
using FarMan.Controllers;
using FarMan.Contents;
using FarMan.Contents.FileRepresentation;
using System.Drawing;
using System.IO;
using FarMan.ConsoleHelpers;
using System.Threading;

namespace FarMan
{
    partial class Program
    {
        static void Main(string[] args)
        {
            InitializeView();

            ConsoleExpander.DrawImage("../imgs/intro.png", 0, 0);
            Thread.Sleep(4000);
            ConsoleExpander.ClearConsole(0, 0);

            List<Controller> controllers = new List<Controller>();

            #region file list controllers, content, panel    
            var fileListPanel = new Panel(
                    InitParams.Location[FarLocations.PanelFileListenerLocation],
                    InitParams.Size[FarSizes.PanelFileListenerSize],
                    "File list"
            );
            fileListPanel.Initialize();
          

            FileList fileListContent = new FileList(fileListPanel);
            #endregion

            #region file info controllers, content, panel
            var fileInfoPanel = new Panel(
                    InitParams.Location[FarLocations.PanelFileDataLocation],
                    InitParams.Size[FarSizes.PanelFileDataSize],
                    "File Info"
                );
            fileInfoPanel.Initialize();
            fileInfoPanel.DrawShape();

            FileCreatingContent fcc = new FileCreatingContent(fileInfoPanel);
            fcc.Initialize();

            FileInfoContent fileInfoView = new FileInfoContent(fileInfoPanel);

            #endregion

            #region file controllers, content, panel
            var fileRepresentaionPanel = new Panel(
                InitParams.Location[FarLocations.PanelFileViewLocation],
                InitParams.Size[FarSizes.PanelFileViewSize],
                "File"
            );
            fileRepresentaionPanel.Initialize();
            fileRepresentaionPanel.DrawShape();

            //TODO: In the separate file
            FileImageContent fileImageContent = new FileImageContent(fileRepresentaionPanel);

            #endregion

            SystemInfoIconsContent iconsContent = new SystemInfoIconsContent(fileRepresentaionPanel);
            iconsContent.Initialize();
            MusicPlayerContent mp = new MusicPlayerContent();
            mp.Initialize();

            EventRegistrator erInstance = EventRegistrator.Instance;
            erInstance.RegisterNewItemPanel(fileRepresentaionPanel);
            erInstance.RegisterNewItemPanel(fileInfoPanel);
            erInstance.RegisterNewItemPanel(fileListPanel);
            erInstance.DefaultPanel = fileListPanel;
            fileListPanel.Active = true;

            controllers.Add(new Controller(fileListContent));
            controllers.Add(new Controller(fileImageContent));
            controllers.Add(new Controller(fileInfoView));
            int currentController = 0;
            while (true)
            {
                ConsoleKeyInfo a = Console.ReadKey(false);
                if(a.Key == ConsoleKey.DownArrow)
                    controllers[currentController].KeyDown();
                if (a.Key == ConsoleKey.UpArrow)
                    controllers[currentController].KeyUp();
                if (a.Key == ConsoleKey.Enter)
                    controllers[currentController].KeyEnter();
                if (a.Key == ConsoleKey.Tab)
                    currentController = (currentController + 1) % controllers.Count;
                if (a.Key == ConsoleKey.F2)
                    controllers[currentController].KeyF2();
                if (a.Key == ConsoleKey.F3)
                    controllers[currentController].KeyF3();
            }
        }

    }
}

