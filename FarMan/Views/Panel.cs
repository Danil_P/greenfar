﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarMan.HelpStructures;
using FarMan.ConsoleHelpers;
using FarMan.FarEnums;
using FarMan.Interfaces;

namespace FarMan.Panels
{
    public partial class Panel : IInitializable
    {
        public void Initialize()
        {
            BorderColor = InitParams.Colors[FarColors.BorderColor];
            ActiveBorderColor = InitParams.Colors[FarColors.ActiveBorderColor];
            BackroundColor = InitParams.Colors[FarColors.BackgroundColor];
            TitleColor = InitParams.Colors[FarColors.BorderTitleColor];

            var ver = InitParams.BorderSign[FarEnums.BorderSigns.Vertical];
            var hor = InitParams.BorderSign[FarEnums.BorderSigns.Horizontal];
            var leftBott = InitParams.BorderSign[FarEnums.BorderSigns.LeftBottom];
            var leftTop = InitParams.BorderSign[FarEnums.BorderSigns.LeftTop];
            var rightTop = InitParams.BorderSign[FarEnums.BorderSigns.RightTop];
            var rightBott = InitParams.BorderSign[FarEnums.BorderSigns.RightBottom];
            BorderSigns = new BorderCharacters(ver, hor, leftTop, leftBott, rightTop, rightBott);

        }
    }
}
