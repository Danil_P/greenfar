﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarMan.Panels;
using FarMan.HelpStructures;
using FarMan.FarEnums;
using FarMan.Interfaces;

namespace FarMan
{
    partial class Program
    {
        public static void InitializeView()
        {
            Size windowSize = InitParams.Size[FarSizes.AppSize];
            Console.SetWindowSize(windowSize.width, windowSize.height);//92 25

            Console.BackgroundColor = InitParams.Colors[FarColors.BackgroundColor];
            Console.Clear();
        }
    }
}
